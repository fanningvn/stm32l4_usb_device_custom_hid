/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/

#ifndef __UART_H__
#define __UART_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#define  USART_ISR_TXE                       ((uint32_t)0x00000080)            /*!< Transmit Data Register Empty */



extern void xputchar(uint8_t c);
void xputchar_2(uint8_t c);
extern void USART_SendData(USART_TypeDef* USARTx, uint16_t Data);
extern FlagStatus USART_GetFlagStatus(USART_TypeDef* USARTx, uint32_t USART_FLAG);

#ifdef __cplusplus
}
#endif

#endif //__UART_H__
