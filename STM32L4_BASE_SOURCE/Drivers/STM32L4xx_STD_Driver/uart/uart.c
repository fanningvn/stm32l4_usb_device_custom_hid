/**
 ******************************************************************************
 * @Author: ThanNT
 * @Date:   13/08/2016
 ******************************************************************************
**/
#include "stm32l4xx_hal.h"
#include "stm32l4r5xx.h"

#include "../../common/cmd_line.h"
#include "../../common/utils.h"
#include "../../common/xprintf.h"

void USART_SendData(USART_TypeDef* USARTx, uint16_t Data) {
	/* Transmit Data */
	USARTx->TDR = (Data & (uint16_t)0x01FF);
}

FlagStatus USART_GetFlagStatus(USART_TypeDef* USARTx, uint32_t USART_FLAG) {
	FlagStatus bitstatus = RESET;

	if ((USARTx->ISR & USART_FLAG) != (uint16_t)RESET) {
		bitstatus = SET;
	}
	else {
		bitstatus = RESET;
	}
	return bitstatus;
}

void xputchar(uint8_t c) {
	USART_SendData(USART3, (uint8_t)c);
	while (USART_GetFlagStatus(USART3, USART_ISR_TC) == RESET);
}

void xputchar_2(uint8_t c) {
	USART_SendData(USART2, (uint8_t)c);
	while (USART_GetFlagStatus(USART2, USART_ISR_TC) == RESET);
}
