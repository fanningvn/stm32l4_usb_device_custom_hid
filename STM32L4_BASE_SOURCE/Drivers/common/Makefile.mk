CFLAGS          += -I./Drivers/common
CPPFLAGS	+= -I./Drivers/common

VPATH += Drivers/common

C_SOURCES += Drivers/common/utils.c
C_SOURCES += Drivers/common/xprintf.c
C_SOURCES += Drivers/common/cmd_line.c
C_SOURCES += Drivers/common/log_queue.c
C_SOURCES += Drivers/common/fifo.c
C_SOURCES += Drivers/common/ring_buffer.c
