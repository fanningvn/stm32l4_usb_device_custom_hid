#stm32 libs path
CFLAGS          += -I./Drivers/arduino/port/cores/stm32
CPPFLAGS	+= -I./Drivers/arduino/port/cores/stm32

VPATH += Drivers/arduino/port/cores/stm32

SOURCES += Drivers/arduino/port/cores/stm32/dtostrf.c
SOURCES += Drivers/arduino/port/cores/stm32/hooks.c
