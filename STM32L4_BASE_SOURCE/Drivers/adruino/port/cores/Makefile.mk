-include Drivers/arduino/port/cores/stm32/Makefile.mk

# header path
CFLAGS          += -I./Drivers/arduino/port/cores
CPPFLAGS	+= -I./Drivers/arduino/port/cores
# sources path
VPATH += Drivers/arduino/port/cores

# cpp files
CPP_SOURCES += Drivers/arduino/port/cores/wiring_digital.cpp
CPP_SOURCES += Drivers/arduino/port/cores/wiring_shift.cpp
CPP_SOURCES += Drivers/arduino/port/cores/Print.cpp
CPP_SOURCES += Drivers/arduino/port/cores/Stream.cpp
CPP_SOURCES += Drivers/arduino/port/cores/WString.cpp
CPP_SOURCES += Drivers/arduino/port/cores/WMath.cpp
CPP_SOURCES += Drivers/arduino/port/cores/itoa.cpp
CPP_SOURCES += Drivers/arduino/port/cores/IPAddress.cpp
